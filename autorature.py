#!usr/bin/env python3
# -*- coding: utf-8 -*-

import fontforge as ff
import glob
import lxml.etree as ET
import random
import re

from time import sleep

font = ff.open('EBGaramond-Simple.sfd')
avg_width = 430
space_width = 100

svgs = sorted(glob.glob('svgs/taches/taches-b/*.svg')) # le sorted() est important parce que les fichiers sont complètements mélangés (leur ordre dépend du filesystem et pas de leur nom)

for i, glyph in enumerate(svgs):
    with open(glyph, 'rt') as f:
        treeLet = ET.parse(f)
        rootLet = treeLet.getroot()
        chasse = rootLet.get('width')
        # viewBox = rootLet.get('viewBox')
        # dimensions = viewBox.split(' ')
        # viewBox = '%s %s %s 1000' % (dimensions[0], dimensions[1], dimensions[2])
        # rootLet.set('viewBox', viewBox)
        # nvb = rootLet.get('viewBox')
        #
        # height = rootLet.get('height')
        # print(nvb, height)

    # --- TÂCHES --- #
    ### construit les glyphes dans une PUA
    new_letter = font.createChar(62530 + i)
    new_letter.importOutlines(glyph)
    new_letter.left_side_bearing = avg_width * -1
    new_letter.width = 0

font.generateFeatureFile('feature_temp.fea')

### Génère les lookups de substitution pour tous les glyphes concernés
chars = ['\\exclam', '\\numbersign', '\\dollar', '\\percent', '\\ampersand', '\\asterisk', '\\plus', '\\zero', '\\one', '\\two', '\\three', '\\four', '\\five', '\\six', '\\seven', '\\eight', '\\nine', '\\less', '\\equal', '\\greater', '\\question', '\\at', '\\A', '\\B', '\\C', '\\D', '\\E', '\\F', '\\G', '\\H', '\\I', '\\J', '\\K', '\\L', '\\M', '\\N', '\\O', '\\P', '\\Q', '\\R', '\\S', '\\T', '\\U', '\\V', '\\W', '\\X', '\\Y', '\\Z', '\\asciicircum', '\\grave', '\\a', '\\b', '\\c', '\\d', '\\e', '\\f', '\\g', '\\h', '\\i', '\\j', '\\k', '\\l', '\\m', '\\n', '\\o', '\\p', '\\q', '\\r', '\\s', '\\t', '\\u', '\\v', '\\w', '\\x', '\\y', '\\z', '\\asciitilde', '\\exclamdown', '\\cent', '\\sterling', '\\currency', '\\yen', '\\section', '\\copyright', '\\guillemotleft', '\\registered', '\\plusminus', '\\mu', '\\paragraph', '\\guillemotright', '\\onequarter', '\\onehalf', '\\threequarters', '\\questiondown', '\\Agrave', '\\Aacute', '\\Acircumflex', '\\Atilde', '\\Adieresis', '\\Aring', '\\AE', '\\Ccedilla', '\\Egrave', '\\Eacute', '\\Ecircumflex', '\\Edieresis', '\\Igrave', '\\Iacute', '\\Icircumflex', '\\Idieresis', '\\Eth', '\\ntilde', '\\Ograve', '\\Oacute', '\\Ocircumflex', '\\Otilde', '\\Odieresis', '\\multiply', '\\Oslash', '\\Ugrave', '\\Uacute', '\\Ucircumflex', '\\Udieresis', '\\Yacute', '\\germandbls', '\\agrave', '\\aacute', '\\acircumflex', '\\atilde', '\\adieresis', '\\aring', '\\ae', '\\ccedilla', '\\egrave', '\\eacute', '\\ecircumflex', '\\edieresis', '\\igrave', '\\iacute', '\\icircumflex', '\\idieresis', '\\eth', '\\ntilde', '\\ograve', '\\oacute', '\\ocircumflex', '\\otilde', '\\odieresis', '\\divide', '\\oslash', '\\ugrave', '\\uacute', '\\ucircumflex', '\\udieresis']

with open('feature_temp.fea', 'r') as f:
    imp = f.read()

with open('fifi.fea', 'w') as file:
    for i, line in enumerate(imp.splitlines()):
        if i not in range(627, 644):
            file.write(line+'\n')
    file.write('lookup ss01Latin {\n    lookupflag 0;\n')
    for char in chars:
        chiffre = random.randint(66, 65 + len(svgs)) # Choisit une tache aléatoirement pour chaque caractère "raturable"
        chiffre_hex = str(hex(chiffre).lstrip('0x')) # Convertit la quantité de taches en nombre hexadécimal pour avoir un code Unicode valide
        file.write('       sub {0} by {0} \\uniF4{1};\n'.format(char, chiffre_hex.upper()))
    file.write('} ss01Latin;\n\n')
    with open('features_template.txt') as template:
        file.write(template.read())
print('>>> Yeah c\'est fini')

font.mergeFeature('fifi.fea')
version = 2
font.encoding = 'Compacted'
font.save('process-tache/taches/b/garature_taches_b%s.sfd' % version)

# --- BARRE-DIAGONALE --- #
### construit les glyphes dans une PUA
# new_letter = font.createChar(62530 + i)
# new_letter.importOutlines(glyph)
# new_letter.left_side_bearing = (i + 1) * avg_width * -1
# new_letter.right_side_bearing = space_width
