#!usr/bin/env python3
# -*- coding: utf-8 -*-

import random
import fontforge as ff
import glob

font = ff.open('EBGaramond-Simple.sfd')

svgs = sorted(glob.glob('svgs/taches/gribouille-a/*.svg'))

### Pour faire la liste des caractères dans un format "featurable"
# with open('charlist.txt', 'w') as recap:
#     recap.write('') # nettoie le fichier
#
# with open('charlist.txt', 'a') as recap:
#     for glyph in font.__iter__(): # ajoute le nom de tous les caractères de la fonte
#         recap.write('\\%s ' % glyph)
#         print('\\%s ' % glyph)

### Génère les lookups de substitution pour tous les glyphes concernés
chars = ['\\exclam', '\\numbersign', '\\dollar', '\\percent', '\\ampersand', '\\asterisk', '\\plus', '\\zero', '\\one', '\\two', '\\three', '\\four', '\\five', '\\six', '\\seven', '\\eight', '\\nine', '\\less', '\\equal', '\\greater', '\\question', '\\at', '\\A', '\\B', '\\C', '\\D', '\\E', '\\F', '\\G', '\\H', '\\I', '\\J', '\\K', '\\L', '\\M', '\\N', '\\O', '\\P', '\\Q', '\\R', '\\S', '\\T', '\\U', '\\V', '\\W', '\\X', '\\Y', '\\Z', '\\asciicircum', '\\grave', '\\a', '\\b', '\\c', '\\d', '\\e', '\\f', '\\g', '\\h', '\\i', '\\j', '\\k', '\\l', '\\m', '\\n', '\\o', '\\p', '\\q', '\\r', '\\s', '\\t', '\\u', '\\v', '\\w', '\\x', '\\y', '\\z', '\\asciitilde', '\\exclamdown', '\\cent', '\\sterling', '\\currency', '\\yen', '\\section', '\\copyright', '\\guillemotleft', '\\registered', '\\plusminus', '\\mu', '\\paragraph', '\\guillemotright', '\\onequarter', '\\onehalf', '\\threequarters', '\\questiondown', '\\Agrave', '\\Aacute', '\\Acircumflex', '\\Atilde', '\\Adieresis', '\\Aring', '\\AE', '\\Ccedilla', '\\Egrave', '\\Eacute', '\\Ecircumflex', '\\Edieresis', '\\Igrave', '\\Iacute', '\\Icircumflex', '\\Idieresis', '\\Eth', '\\ntilde', '\\Ograve', '\\Oacute', '\\Ocircumflex', '\\Otilde', '\\Odieresis', '\\multiply', '\\Oslash', '\\Ugrave', '\\Uacute', '\\Ucircumflex', '\\Udieresis', '\\Yacute', '\\germandbls', '\\agrave', '\\aacute', '\\acircumflex', '\\atilde', '\\adieresis', '\\aring', '\\ae', '\\ccedilla', '\\egrave', '\\eacute', '\\ecircumflex', '\\edieresis', '\\igrave', '\\iacute', '\\icircumflex', '\\idieresis', '\\eth', '\\ntilde', '\\ograve', '\\oacute', '\\ocircumflex', '\\otilde', '\\odieresis', '\\divide', '\\oslash', '\\ugrave', '\\uacute', '\\ucircumflex', '\\udieresis']

with open('featurelist.txt', 'w') as f:
    for char in chars:
        chiffre = random.randint(66, 65 + len(svgs)) # Choisit une tache aléatoirement pour chaque caractère "raturable"
        chiffre_hex = str(hex(chiffre).lstrip('0x')) # Convertit la quantité de taches en nombre hexadécimal pour avoir un code Unicode valide
        # f.write('sub {0} by {0} \\uniF4{1};\n'.format(char, chiffre_hex.upper()))
        print(' ' + chiffre_hex.upper())
print('>>> Yeah c\'est fini')
