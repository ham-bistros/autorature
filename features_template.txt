feature ss01 {

 script DFLT;
     language dflt ;
      lookup ss01Latin;

 script latn;
     language dflt ;
      lookup ss01Latin;
} ss01;
